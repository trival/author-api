module.exports = function(server) {
    console.log('Creating client bundle...');

    var fs = require('fs'),
        browserify = require('browserify'),
        boot = require('loopback-boot'),
        uglify = require('uglify-js'),
        appDir = __dirname + '/../../client/';


    var b = browserify({
        basedir: appDir,
    });

    // add the main application file
    b.add('./client.js');

    // add boot instructions
    boot.compileToBrowserify(appDir, b);

    // create the bundle
    var out = fs.createWriteStream(appDir + '../public/loopback-client.js');
    b.bundle().pipe(out);
    out.on('error', function() {
        console.log(arguments);
    });
    out.on('finish', function() {
        var minout = fs.createWriteStream(appDir + '../public/loopback-client.min.js'),
            min = uglify.minify(appDir + '../public/loopback-client.js');
            minout.end(min.code);
        out.close();
    });
    // handle out.on('error') and out.on('close')
};
